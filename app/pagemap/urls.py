# -*- coding: utf-8 -*-
__author__ = 'Tony Adam'

from django.conf import settings
from django.conf.urls import include, patterns, url
from django.conf.urls.static import static

from views import PageDetailView

urlpatterns = patterns('',
    url(r'^(?P<slug>[a-zA-Z0-9_-]+)/$', PageDetailView.as_view(), name='detail'),
)
