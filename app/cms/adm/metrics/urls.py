# -*- coding: utf-8 -*-
__author__ = 'Tony Adam'

from django.conf.urls import include, patterns, url
from django.contrib.auth.decorators import login_required

from views import ChangeSettings

urlpatterns = patterns('',
    url(r'^$', login_required(ChangeSettings.as_view()), name='index'),
)
