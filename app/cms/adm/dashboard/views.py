# -*- coding: utf-8 -*-
__author__ = 'Tony Adam'

from django.views.generic import TemplateView


class DashBoardIndex(TemplateView):
    template_name = "adm/dashboard/index.html"
