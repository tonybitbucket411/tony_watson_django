# -*- coding: utf-8 -*-
__author__ = 'Tony Adam'

from django.conf.urls import patterns, url

from views import ClickView

urlpatterns = patterns(
    '',
    url(r'^$', ClickView.as_view(), name='click'),
)
