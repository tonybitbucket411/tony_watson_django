# -*- coding: utf-8 -*-
__author__ = 'Tony Adam'

from django.contrib import admin

from models import AdvPlace


class AdvPlaceAdmin(admin.ModelAdmin):
    pass


admin.site.register(AdvPlace, AdvPlaceAdmin)
